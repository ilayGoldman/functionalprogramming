import java.lang.NumberFormatException
import scala.math.abs

object ZAnomalyDetector extends AnomalyDetector {
  override def learn(normal: TimeSeries): Map[String, String] = {
    var map = Map[String, String]()
    normal.features.foreach(feature => map = map + (feature -> Util.max_abs_zscore(normal.getValues(feature).get.toArray).toString))
    map
  }

  override def detect(model: Map[String, String], test: TimeSeries): Vector[(String, Int)] = {
    var vec = Vector[(String, Int)]()

    model.foreach{case (feature, zValue) =>
      try {
        test.getValues(feature) match {
          case None => None
          case Some(vecFeature) => vecFeature.zipWithIndex.foreach { case (value, index) =>
            if (abs(Util.zscore(vecFeature.toArray, value)) > zValue.toDouble)
              vec = vec :+ (feature, index)
          }
        }
      }
      catch { // if we didnt get double in the model values
        case e: Exception => None
      }
    }

    vec
  }

}
