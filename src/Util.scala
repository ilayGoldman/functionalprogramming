import scala.collection.immutable.List
import scala.math.abs

object    Util {

	// max
  def max [A](list: List[A], func: (A,A) => Int) : A = {
    // what if the list is empty, do we need to do something??
    list.filter(item => list.filter(secondItem => func(item,secondItem) < 0).isEmpty)(0)
  }

	// map
  def map [A,B,C](list: List[A], func: (A) => B, func2: (B) => C)  : List[C]  = {
    list.map(item => func2(func(item))) : List[C]
  }

	// isSorted
  def isSorted [A](list: List[A], func: (A,A) => Boolean)  : Boolean  = {
    if (list.tail.isEmpty) true
    else if (func(list.head, list.tail.head)) isSorted(list.tail,func)
    else false

  }
	// probs
  def probs (xs: Array[Double])  : Array[Double]  = {
    xs.map(item => xs.count(i => i==item).toDouble/xs.length)
  }

	// entropy
  def entropy (xs: Array[Double])  : Double  = {
    //probs(xs).map((item) => -1*item*scala.math.log10(item)/scala.math.log10(2.0)).foldLeft(0.0)(_+_)
    xs.groupBy(item => item).map(item => (item._2.length.toDouble/xs.length)*(-1).toDouble*scala.math.log10(item._2.length.toDouble/xs.length)/scala.math.log10(2.0)).foldLeft(0.0)(_+_)
  }

	// mu
  def mu (xs: Array[Double])  : Double  = {
    xs.groupBy(item => item).map(item => (item._2.length.toDouble/xs.length) * item._1).foldLeft(0.0)(_+_)
  }
	// variance
  def variance (xs: Array[Double])  : Double  = {
    xs.groupBy(item => item).map(item => (item._2.length.toDouble/xs.length) * scala.math.pow(item._1 - mu(xs),2)).foldLeft(0.0)(_+_)
  }
	// zscore.
  def zscore (xs: Array[Double], x: Double)  : Double  = {
    (x-mu(xs))/scala.math.sqrt(variance(xs))
  }
	// cov
  def cov (xs: Array[Double], ys: Array[Double])  : Double  = {
    mu(xs.zip(ys).map(item => item._1 * item._2)) - (mu(xs) * mu(ys))
  }

	// pearson
  def pearson (xs: Array[Double], ys: Array[Double])  : Double  = {
    cov(xs,ys) / (scala.math.sqrt(variance(xs))*scala.math.sqrt(variance(ys)))
  }

  def max_abs_zscore(xs: Array[Double]) : Double = {
    abs(zscore(xs,xs.maxBy(x => abs(zscore(xs, x)))))
  }

  def getMaxPearson(normal: TimeSeries, feature: String, restOfFeatures: Array[String]): (String, String, Double) =
  {
    val featValues = normal.getValues(feature).get.toArray
    val maxFeature = restOfFeatures.maxBy(yFeature => abs(Util.pearson(featValues, normal.getValues(yFeature).get.toArray)))

    (feature, maxFeature, Util.pearson(featValues, normal.getValues(maxFeature).get.toArray))
  }
 /*def checkModelValid(test: TimeSeries, features: String) : (Vector[Double], Vector[Double]) = {
   val (featureA, featureB) = features.split(',') match {
     case Array(a, b) => (a, b)
     case _ =>  (None,None)
   }

   if (featureA == None || featureB ==None) {
     (Nothing,NothingClass)
   }
   else {

     val featureAValues = test.getValues(featureA) match {
       case Some(p) => p
       case None => None
     }

     val featureBValues = test.getValues(featureB) match {
       case Some(p) => p
       case None => None
     }

     if (featureAValues != None && featureBValues != None) {
       (featureAValues, featureBValues)
     }
     else {
       (None, None)
     }
   }
 }*/
}
