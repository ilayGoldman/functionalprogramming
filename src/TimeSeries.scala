import sun.awt.SunToolkit.{OperationTimedOut, postEvent, postPriorityEvent}

import scala.collection.immutable.HashMap
import scala.io.Source
import scala.collection.immutable.List
import scala.collection.mutable.ListBuffer

class TimeSeries() {

  var hashTimeSeries: HashMap[String, Vector[Double]] = HashMap.empty[String, Vector[Double]]

  def this(csvFileName:String) {
    this()
    val source=Source.fromFile(csvFileName)
    val fileIterator = source.getLines()

    // the features
    val A = fileIterator.next().split(',')

    // val features = A.toVector
    A.foreach(feature => hashTimeSeries += (feature -> Vector[Double]()))

    // the data
    fileIterator.foreach(line=>{
      A.zip(line.split(',')).foreach(tuple =>  hashTimeSeries =  hashTimeSeries + (tuple._1 -> (hashTimeSeries.get(tuple._1).get :+ tuple._2.toDouble)))
    })
    source.close()
  }

  def this (vec: Vector[(String, Vector[Double])]) {
    this()

    vec.foreach(item => hashTimeSeries += (item._1 -> item._2))
  }

  //val features=hashTimeSeries.keys.toVector
  def features(): Vector[String] = hashTimeSeries.keys.toVector

  // given name of a feature return in O(1) its value series
  def getValues(feature:String):Option[Vector[Double]]=hashTimeSeries.get(feature)

  // given name of a feature return in O(1) its value at the given time step
  def getValue(feature:String,timeStep:Int):Option[Double]=
    getValues(feature) match {
      case None => None
      case Some(p) => p.lift(timeStep)
    }

  // given name of a feature return its value series in the range of indices
  def getValues(feature:String,r:Range):Option[Vector[Double]]=
    getValues(feature) match {
      case Some(p) => p.lift(r.end) match {
        case None => None
        case Some(q) => if (r.start < 0) None else Option(r.map(item => getValue(feature, item).get).toVector) //the feature exists and the range is ok
      }
      case None => None
    }

  def length() : Int = hashTimeSeries.get(features.head).get.length

  def split(n: Int) : List[TimeSeries] = {
    val chunkSize = this.length() / n
    var listBuf = ListBuffer[TimeSeries]()

    val range = chunkSize to (chunkSize * n) - 1 by chunkSize
    var startOfLast = 0
    range.foreach(i =>
      listBuf += new TimeSeries(this.features.map(feature => (feature, this.getValues(feature, i-chunkSize to i-1).get)))
    )

    if (!range.isEmpty)
      startOfLast = range.last

    // add the last portion of the extra if needed
    listBuf += new TimeSeries(this.features.map(feature => (feature, this.getValues(feature, startOfLast to this.length()-1).get)))

    // return Immutable List
    listBuf.toList
  }

}
