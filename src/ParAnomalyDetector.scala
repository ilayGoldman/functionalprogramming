import java.util.concurrent.{Callable, ExecutorService, Future}
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent._



case class Report(feature: String, var timeStep: Int, anomalyScore: Double)


trait ParAnomalyDetector {
  type Reports = ListBuffer[Report]
  def map(ts: TimeSeries): Reports
  def reduce(r1: Reports, r2: Reports): Reports

  // implement
  def detect(ts: TimeSeries, es: ExecutorService, chunks: Int): Vector[Report] = {

    val chunkSize = ts.length() / chunks
    val list = ts.split(chunks)

    val listFut = list.map(timeSer => es.submit(new Callable[Reports] {override def call(): Reports = map(timeSer)}))

    val listRep = listFut.map(fut => fut.get())

    // fix positions because we split the timeSeries
    val updatedListRep = listRep.zipWithIndex.map(item => item._1.map(report => new Report(report.feature,(chunkSize * item._2) + report.timeStep, report.anomalyScore)))

    // check here if it works because of the "new Reports" which is weird
    updatedListRep.foldLeft(updatedListRep.head)(reduce(_,_)).toVector

  }
}

