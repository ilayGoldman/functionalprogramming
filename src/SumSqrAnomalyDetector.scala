
import scala.collection.mutable
import scala.math.{abs, pow, sqrt}

object SumSqrAnomalyDetector extends  AnomalyDetector {
  override def learn(normal: TimeSeries): Map[String, String] = {
    var map = Map[String, String]()
    normal.features.zipWithIndex.foreach{case (feature, index) =>
      // check here that it works with last element and the opne before that (when the foreach is on D it has E)
      if ((index+1) != normal.features.length) {
        val (featureA, maxFeature, pearson) = Util.getMaxPearson(normal, feature, normal.features.slice(index + 1, normal.features.length).toArray)

        if (abs(pearson) >= 0.9) {
          // this is the line, we need to see what we do about this
          val PointVec = normal.getValues(feature).get.zip(normal.getValues(maxFeature).get).map{case (x,y) => new Point(x,y)}


          val maxDist = getMaxSqureSum(PointVec)
          val key = feature+","+maxFeature
          // ("FeatureA,FeatureB", "dist")
          map = map + (key -> maxDist.toString)
        }
      }
    }

    map
  }

  override def detect(model: Map[String, String], test: TimeSeries): Vector[(String, Int)] = {
    var vec = Vector[(String, Int)]()
    model.foreach { case (features, lines) =>
      val (featureA, featureB) = features.split(',') match {
        case Array(a, b) => (a, b)
        case _ => return Vector[(String, Int)]()
      }

      val featureAValues = test.getValues(featureA) match {
        case Some(p) => p
        case None => None
      }

      val featureBValues = test.getValues(featureB) match {
        case Some(p) => p
        case None => None
      }
      if (featureAValues != None && featureBValues != None) {

        // extract the line and max dist
        try {
          val maxDist = lines.toDouble

          val PointVec = featureAValues.asInstanceOf[Vector[Double]].zip(featureBValues.asInstanceOf[Vector[Double]]).map { case (x, y) => new Point(x.asInstanceOf[Double], y.asInstanceOf[Double]) }

          val anomalies = PointVec.zipWithIndex.filter{case (p, index) => getSqureSum(PointVec, p) > (maxDist + 0.000001) }
          anomalies.foreach{case (p, index) => vec = vec :+ (features, index)}
        }
        catch { // if we didnt get double in the model values
          case e: Exception => None
        }
      }
    }

    vec
  }



  def getSqureSum(xs : Vector[Point], p: Point) : Double = {
    xs.foldRight(0.0)((currP,sum) => sum+pow(getSquareDist(p, currP),2))
  }
  def getMaxSqureSum(xs : Vector[Point]) : Double = {
    val maxP = xs.maxBy(p =>
      // the sum square isnt affected if we leave the current point in the array
      getSqureSum(xs,p)
    )
    getSqureSum(xs,maxP)
  }

  // the sum square isn't affected if we leave the current point in the array
  def getMinSqureSumPoint(xs : Vector[Point]) : Point = xs.minBy(p =>getSqureSum(xs,p))

  def getSquareDist(p1: Point, p2:Point) : Double = {
    sqrt(pow(p1.x-p2.x,2) + pow(p1.y-p2.y,2))
  }
}
