
class Line(ps:Array[Point], aParam:Double, bParam:Double) {

  def this(ps:Array[Point]) {
    this(ps,
      Util.cov(ps.map(item => (item.x)),ps.map(item => (item.y)))/Util.variance(ps.map(item => (item.x))),
      Util.mu(ps.map(item => (item.y))) - Util.cov(ps.map(item => (item.x)),ps.map(item => (item.y)))/Util.variance(ps.map(item => (item.x))) * Util.mu(ps.map(item => (item.x))));
  }

  def this(a:Double, b:Double) {
    this(Array[Point](), a, b);


  }

  def a = aParam
  def b = bParam
  //val a = 14.901012998672755
  //val b = 4.621003869958088

  // f
  def f (x: Double)  : Double  = {
    (a * x) + b
  }

	// dist
  def dist (p: Point)  : Double  = {
  scala.math.abs(f(p.x) - p.y)
  }
}

/*object LineCreator {
  def createLine(a: Double, b:Double) : Line = {
    val p1 = new Point(1,(a * 1) + b)
    val p2 = new Point(2,(a * 2) + b)
    new Line(Array[Point](p1,p2))
  }
}*/

