import Util.entropy

object EntropyAnomalyDetector extends ParAnomalyDetector{

//
  override def map(ts: TimeSeries):Reports={
    val r = new Reports()
    ts.features().foreach(feature => {
      val list = ts.getValues(feature).get
      val normalEntropy = entropy(list.toArray)
      val anomaly = list.zipWithIndex.maxBy(item =>
        normalEntropy - entropy(list.zipWithIndex.filter(a => a._2 != item._2).map(b => b._1).toArray)
      )
      r += new Report(feature, anomaly._2, normalEntropy - entropy(list.zipWithIndex.filter(a => a._2 != anomaly._2).map(b => b._1).toArray))
    }
    )

    r
  }
//case class Report(feature: String, var timeStep: Int, anomalyScore: Double)
  override def reduce(r1:Reports,r2:Reports):Reports={

    // List of report
    r1.map(item => (r2.filter(item2 => item2.feature==item.feature).toList :+ (item)).maxBy(combined => combined.anomalyScore))
  }
}