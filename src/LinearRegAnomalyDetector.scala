import scala.math.abs

object LinearRegAnomalyDetector extends  AnomalyDetector {
  override def learn(normal: TimeSeries): Map[String, String] = {
    var map = Map[String, String]()
    normal.features.zipWithIndex.foreach{case (feature, index) =>
      // check here that it works with last element and the opne before that (when the foreach is on D it has E)
      if ((index+1) != normal.features.length) {
        val (featureA, maxFeature, pearson) = Util.getMaxPearson(normal, feature, normal.features.slice(index + 1, normal.features.length).toArray)

        if (abs(pearson) >= 0.9) {
          // this is the line, we need to see what we do about this
          val PointArray = normal.getValues(feature).get.zip(normal.getValues(maxFeature).get).map{case (x,y) => new Point(x,y)}.toArray
          val line = new Line(PointArray)
          val maxDist = line.dist(PointArray.maxBy(point => line.dist(point)))
          val key = feature+","+maxFeature
          val value = line.a.toString+","+line.b.toString+","+maxDist.toString

          // ("FeatureA,FeatureB", "a,b,dist")
          map = map + (key -> value)
        }
      }
    }

    map
  }

  override def detect(model: Map[String, String], test: TimeSeries): Vector[(String, Int)] = {
    var vec = Vector[(String, Int)]()
    model.foreach { case (features, lines) =>
      val (featureA, featureB) = features.split(',') match {
        case Array(a, b) => (a, b)
        case _ => return Vector[(String, Int)]()
      }

      val featureAValues = test.getValues(featureA) match {
        case Some(p) => p
        case None => None
      }

      val featureBValues = test.getValues(featureB) match {
        case Some(p) => p
        case None => None
      }
      if (featureAValues != None && featureBValues != None) {

        // extract the line and max dist
        try {
          val (a, b, maxDist) = lines.split(',') match {
            case Array(a, b, maxDist) => (a.toDouble, b.toDouble, maxDist.toDouble)
            case _ => return Vector[(String, Int)]()
          }

          val PointVec = featureAValues.asInstanceOf[Vector[Double]].zip(featureBValues.asInstanceOf[Vector[Double]]).map { case (x, y) => new Point(x, y) }
          val line = new Line(a,b)

          val anomalies = PointVec.zipWithIndex.filter{case (p, index) => line.dist(p) > (maxDist + 0.000001) }
          anomalies.foreach{case (p, index) => vec = vec :+ (features, index)}
          }
        catch { // if we didnt get double in the model values
          case e: Exception => None
        }
      }
    }

    vec
  }

}
