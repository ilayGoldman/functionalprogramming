
import SumSqrAnomalyDetector.{getMinSqureSumPoint, getSquareDist}

import scala.collection.mutable
import scala.math.abs

object HybridAnomalyDetector extends  AnomalyDetector {

  override def learn(normal: TimeSeries): Map[String, String] = {
    var map = Map[String, String]()
    normal.features.zipWithIndex.foreach { case (feature, index) =>
      // check here that it works with last element and the opne before that (when the foreach is on D it has E)
      if ((index + 1) != normal.features.length) {
        val (featureA, maxFeature, pearson) = Util.getMaxPearson(normal, feature, normal.features.slice(index + 1, normal.features.length).toArray)

        // WE DO LINEAR
        if (abs(pearson) >= 0.9) {
          // this is the line, we need to see what we do about this
          val PointArray = normal.getValues(feature).get.zip(normal.getValues(maxFeature).get).map { case (x, y) => new Point(x, y) }.toArray
          val line = new Line(PointArray)
          val maxDist = line.dist(PointArray.maxBy(point => line.dist(point)))
          val key = "linear," + feature + "," + maxFeature
          val value = line.a.toString + "," + line.b.toString + "," + maxDist.toString

          // ("linear,FeatureA,FeatureB", "a,b,dist")
          map = map + (key -> value)
        }
        else if (abs(pearson) > 0.5) {
          // get the min squre sum POINT,
          val PointVec = normal.getValues(feature).get.zip(normal.getValues(maxFeature).get).map { case (x, y) => new Point(x, y) }
          val minDistPoint = getMinSqureSumPoint(PointVec)
          val radius = getSquareDist(PointVec.maxBy(p => getSquareDist(p, minDistPoint)), minDistPoint)


          val key = "sumSqr," + feature + "," + maxFeature
          val value = radius.toString
          // ("sumSqr,FeatureA,FeatureB", "maxDist")
          map = map + (key -> value)
        }
        else {
          val max_z_score = Util.max_abs_zscore(normal.getValues(feature).get.toArray)
          val key = "z," + featureA + ",StructureKeeper"
          val value = max_z_score.toString

          // ("z,Feature,StructureKeeper", "max_z_abs")
          map = map + (key -> value)
        }
      }
      // only 1 feature left
      else
      {
        val max_z_score = Util.max_abs_zscore(normal.getValues(feature).get.toArray)
        val key = "z," + feature + ",StructureKeeper"
        val value = max_z_score.toString

        // ("z,Feature,StructureKeeper", "max_z_abs")
        map = map + (key -> value)
      }
    }

    map
  }

  override def detect(model: Map[String, String], test: TimeSeries): Vector[(String, Int)] = {
    var vec = Vector[(String, Int)]()
    model.foreach { case (features, lines) =>
      try {
        val (typeOfAlgo, featureA, featureB) = features.split(',') match {
          case Array(a, b, c) => (a, b, c)
          case _ => return Vector[(String, Int)]()
        }

        // we do them together so we wont check twice
        if (typeOfAlgo == "linear" || typeOfAlgo == "sumSqr") {
          val featureAValues = test.getValues(featureA) match {
            case Some(p) => p
            case None => None
          }

          val featureBValues = test.getValues(featureB) match {
            case Some(p) => p
            case None => None
          }
          if (featureAValues != None && featureBValues != None) {
            if (typeOfAlgo == "linear") {
              val (a, b, maxDist) = lines.split(',') match {
                case Array(a, b, maxDist) => (a.toDouble, b.toDouble, maxDist.toDouble)
                case _ => return Vector[(String, Int)]()
              }

              val PointVec = featureAValues.asInstanceOf[Vector[Double]].zip(featureBValues.asInstanceOf[Vector[Double]]).map { case (x, y) => new Point(x, y) }
              val line = new Line(a, b)

              val anomalies = PointVec.zipWithIndex.filter { case (p, index) => line.dist(p) > (maxDist + 0.000001) }
              anomalies.foreach { case (p, index) => vec = vec :+ (featureA+","+featureB, index) }
            }
            // "sumSqr"
            else {
              val modelRadius = lines.toDouble
              val vecFeatureAValues = featureAValues.asInstanceOf[Vector[Double]]

              // get center of circle
              val PointVec = vecFeatureAValues.zip(featureBValues.asInstanceOf[Vector[Double]]).map { case (x, y) => new Point(x.asInstanceOf[Double], y.asInstanceOf[Double]) }
              val circleCenter = getMinSqureSumPoint(PointVec)


              val anomalies = PointVec.zipWithIndex.filter { case (p, index) => getSquareDist(p, circleCenter) > modelRadius }
              anomalies.foreach { case (p, index) => vec = vec :+ (featureA+","+featureB, index) }
            }
          }
        }
        else if (typeOfAlgo == "z") {
          val featureValues = test.getValues(featureA) match {
            case Some(p) => p
            case None => None
          }

          if (featureValues != None) {
            val vecFeature = featureValues.asInstanceOf[Vector[Double]]
            val zValue = lines.toDouble
            vecFeature.zipWithIndex.foreach { case (value, index) =>
              if (abs(Util.zscore(vecFeature.toArray, value)) > zValue.toDouble)
                vec = vec :+ (featureA, index)
            }
          }
        }
      }
      catch { // if we didnt get double in the model values
        case e: Exception => None
      }
    }
    vec
  }
}
